#include <sys/types.h>

void make_crc_table(unsigned int crc_table[]);
unsigned int crc32(unsigned char *p, size_t n);