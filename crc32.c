#include "crc32.h"

void make_crc_table(unsigned int crc_table[]) {
    unsigned int POLYNOMIAL = 0xEDB88320;
    unsigned int remainder;
    unsigned char b = 0;
    do {
        remainder = b;
        for (unsigned int bit = 8; bit > 0; --bit) {
            if (remainder & 1)
                remainder = (remainder >> 1) ^ POLYNOMIAL;
            else
                remainder = (remainder >> 1);
        }
        crc_table[(size_t)b] = remainder;
    } while(0 != ++b);
}

unsigned int crc32(unsigned char *p, size_t n)
{	static unsigned int crc_table[256];
    if(crc_table[sizeof(crc_table) / sizeof(*crc_table) - 1] == 0)
    	make_crc_table(crc_table);	
	unsigned int crc = 0xfffffffful;
    size_t i;
    for(i = 0; i < n; i++)
        crc = crc_table[*p++ ^ (crc&0xff)] ^ (crc>>8);
    return(~crc);
}
