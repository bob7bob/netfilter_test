all : netfilter_test

netfilter_test: main.o crc32.o
	g++ -g -o netfilter_test main.o crc32.o -lnetfilter_queue

main.o: crc32.o
	g++ -g -c -o main.o netfilter_test.c -lnetfilter_queue

crc32.o:
	g++ -g -c -o crc32.o crc32.c

clean:
	rm -f netfilter_test
	rm -f *.o

