
#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <linux/netfilter.h>		/* for NF_ACCEPT */
#include <errno.h>

#include <libnetfilter_queue/libnetfilter_queue.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>

#include <set>

#include "crc32.h"


const char kBlacklistFileName[32] = "urls.txt";
std::set <unsigned int> hashes;


int read_blacklist(const char *filename, std::set <unsigned int> &hashes)
{
    FILE *fp = fopen(filename, "r");
	if(!fp)
	{
		perror("open error");
		exit(-1);
	}
    while(!feof(fp))
    {
        char line[256] = {0};
        fgets(line, sizeof(line), fp);
        char *end = strchr(line, '\n');
        if(end)
            *end = '\0';
        unsigned int hash = crc32((unsigned char*)line, strlen(line));
        hashes.insert(hash);
    }
	fclose(fp);
    return 1;
}

int check_blacklist(char *value, std::set<unsigned int> &hashes)
{
    unsigned int hash = crc32((unsigned char*)value, strlen(value));
	return hashes.find(hash) != hashes.end();
}

int parse_header(char *data, const char *find_header, char *out_value, size_t value_size) //prevent overflow
{
	char *header = 0;
    header = strstr(data, "HTTP/"); //HTTP check
	if(!header) return 0;
    header = strstr(header, "\n");
    if(!header) return 0;
    header = strstr(header, find_header);
    if(!header) return 0;
    char *begin_value = strstr(header, ":");
    if(!begin_value) return 0;
    begin_value++;
    char *end_value = strstr(begin_value, "\n");
    if(!end_value) return 0;

    //strip space
    int j = 0;
    for(int i = 0; i < end_value - begin_value && i < value_size - 1; i++)
    {
        if(!isspace(begin_value[i]))
            out_value[j++] = begin_value[i]; 
    }
    out_value[j] = '\0';
    return 1;
}


u_int8_t* get_tcp_data(u_int8_t *data, u_int32_t *len)
{
	struct ip *ip_header = (struct ip*)(data); 
    struct tcphdr *tcp;
    char ip_version = ip_header->ip_v;
    if(ip_version == 4)
    {
      if(ip_header->ip_p == IPPROTO_TCP)
      {
        size_t ip_header_len = ip_header->ip_hl * 4;
        tcp = (struct tcphdr*)((u_int8_t*)ip_header+ip_header_len);
        size_t tcp_header_len = tcp->th_off * 4;
        u_int8_t *data = (u_int8_t*)tcp + tcp_header_len;

        size_t ip_len = ntohs(ip_header->ip_len);
        
        *len = ip_len - ip_header_len - tcp_header_len;
		return data;;
      }
    }
    return 0;
}

int check_http(char *data)
{
	return strstr(data, "HTTP/1") != NULL;
}

int check_valid_host(char *host)
{
	return check_blacklist(host, hashes) != 1;
}

int monitor_http(char *http_data)
{
	char host[64] = {0};
	parse_header((char*)http_data, "Host", host, sizeof(host));	
	return check_valid_host(host);
}

static int cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg,
	      struct nfq_data *nfa, void *data)
{
	u_int32_t id = 0;
	unsigned char *packet = 0;
	struct nfqnl_msg_packet_hdr *ph = nfq_get_msg_packet_hdr(nfa);
	if (ph) {
		id = ntohl(ph->packet_id);
		printf("hw_protocol=0x%04x hook=%u id=%u \n",
			ntohs(ph->hw_protocol), ph->hook, id);
	}

	u_int32_t payload_len = nfq_get_payload(nfa, &packet);
	if (payload_len >= 0)
		printf("payload_len=%d ", payload_len);
	
	int accept = 1;
	u_int32_t data_len = -1;
	u_int8_t *tcp_data = get_tcp_data(packet, &data_len);
	if(tcp_data && check_http((char*)tcp_data)) {
		char *http_data = (char*)tcp_data;
		accept = monitor_http(http_data);
	}
	accept = accept ? NF_ACCEPT : NF_DROP;
	return nfq_set_verdict(qh, id, accept, 0, NULL);
}


void usage() {
    printf("syntax: netfilter_test <blacklist_filename>\n");
    printf("sample: netfilter_test urls.txt\n");
}

int main(int argc, char **argv)
{
	struct nfq_handle *h;
	struct nfq_q_handle *qh;
	struct nfnl_handle *nh;
	int fd;
	int rv;
	char buf[4096] __attribute__ ((aligned));

	if (argc != 2) {
      usage();
      return -1;
    }
    
	printf("opening library handle\n");
	h = nfq_open();
	if (!h) {
		fprintf(stderr, "error during nfq_open()\n");
		exit(1);
	}

	printf("unbinding existing nf_queue handler for AF_INET (if any)\n");
	if (nfq_unbind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_unbind_pf()\n");
		exit(1);
	}

	printf("binding nfnetlink_queue as nf_queue handler for AF_INET\n");
	if (nfq_bind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_bind_pf()\n");
		exit(1);
	}

	printf("binding this socket to queue '0'\n");
	qh = nfq_create_queue(h,  0, &cb, NULL);
	if (!qh) {
		fprintf(stderr, "error during nfq_create_queue()\n");
		exit(1);
	}

	printf("setting copy_packet mode\n");
	if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
		fprintf(stderr, "can't set packet_copy mode\n");
		exit(1);
	}

	read_blacklist(argv[1], hashes);
	fd = nfq_fd(h);

	for (;;) {
		if ((rv = recv(fd, buf, sizeof(buf), 0)) >= 0) {
			printf("pkt received\n");
			nfq_handle_packet(h, buf, rv);
			continue;
		}
		/* if your application is too slow to digest the packets that
		 * are sent from kernel-space, the socket buffer that we use
		 * to enqueue packets may fill up returning ENOBUFS. Depending
		 * on your application, this error may be ignored. Please, see
		 * the doxygen documentation of this library on how to improve
		 * this situation.
		 */
		if (rv < 0 && errno == ENOBUFS) {
			printf("losing packets!\n");
			continue;
		}
		perror("recv failed");
		break;
	}

	printf("unbinding from queue 0\n");
	nfq_destroy_queue(qh);

#ifdef INSANE
	/* normally, applications SHOULD NOT issue this command, since
	 * it detaches other programs/sockets from AF_INET, too ! */
	printf("unbinding from AF_INET\n");
	nfq_unbind_pf(h, AF_INET);
#endif

	printf("closing library handle\n");
	nfq_close(h);

	exit(0);
}
